import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_new/misrecomendados.dart';
import 'package:http/http.dart' as http;
import 'constants.dart' as globals;
import 'package:expandable/expandable.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'main.dart';


class newrecomendado extends StatefulWidget {
  @override
  newrecomendadoState createState() => newrecomendadoState();
}

class newrecomendadoState extends State<newrecomendado>{
  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold(
      drawer: MenuLateral(),
      appBar: AppBar(
        leading: IconButton(icon:Icon(Icons.arrow_back_ios),
        onPressed:()=>{Navigator.of(context).pushReplacement(MaterialPageRoute(
         builder: (context) => misrecomendados()))},),
        centerTitle: true,
        title: Text("Recomendados"),
      ),
      body: MyCustomFormEdit (),
    );
  }
}

class MyCustomFormEdit extends StatefulWidget {
  @override
  MyCustomFormStateEdit createState() {
    return MyCustomFormStateEdit();
  }
}
// Create a corresponding State class, which holds data related to the form.
class MyCustomFormStateEdit extends State<MyCustomFormEdit> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();
 String nombre;
 String direccion   ;
 String telefono ;

  _Editclient (BuildContext Context) async{

    http.post("${globals.constants.urlApi}api/clientes/saverecomendado",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "direccion": "${direccion}",
          "nombre": "${nombre}",
          "telefono": "${telefono}",
          "idusuario":"${globals.constants.iduser}"
        }
    ).then((http.Response response) {

      String res=response.body;
      var data = jsonDecode(response.body);
      print(data);
      print(res);
      if(data["res"]) {
        globals.showContribuyente(context, "${data['idrecomendado']}");
      }
       /* globals.constants.ejecuto=0;
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => misrecomendados(),
        ));
      }*/
    });
  }

  @override
  Widget build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return
      Form(
        key: _formKey,
        child:
        ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Colors.white,
                  height: 20,
                  thickness: 5,
                  indent: 20,
                  endIndent: 0,
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child:TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.person),
                          hintText: 'Nombre..',
                          labelText: "Nombre..",
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingresa un nombre valido';
                          }
                          return null;
                        },
                        onChanged: (value){
                          nombre=value;
                        },
                      )
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.apps),
                          hintText: 'Dirección...',
                          labelText: 'Dirección',
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingresa una dirección valida';
                          }
                          return null;
                        },
                        onChanged: (value){
                          direccion=value;
                        },
                      )
                  ),
                ) ,
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.email),
                          hintText: 'Telefono...',
                          labelText: 'Telefono',
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingresa un telefono valido';
                          }
                          return null;
                        },
                        onChanged: (value){
                          telefono=value;
                        },
                      )
                  ),
                ),
                new Container(
                    padding: const EdgeInsets.all(7.0),
                    child: Center(
                      child: new RaisedButton(
                        child: const Text('Guardar'),
                        shape:StadiumBorder(),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _Editclient(context);
                            Scaffold.of(context)
                                .showSnackBar(
                                SnackBar(content: Text('Guardando...')));

                          }
                        },
                      ),
                    )),
              ],
            ),
          ],
        ),
      );
  }
}




class editrecomendado extends StatefulWidget {

  var id;
  var nombre;
  var direccion;
  var telefono;

  editrecomendado(this.id,this.nombre,this.direccion,this.telefono);
  @override
  editrecomendadoState createState() => editrecomendadoState();
}

class editrecomendadoState extends State<editrecomendado>{
  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold(

      appBar: AppBar(
      /*  leading: IconButton(icon:Icon(Icons.arrow_back_ios),
          onPressed:()=>{Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => misrecomendados()))},),*/
        centerTitle: true,
        title: Text("Recomendados"),
      ),
      body: Customedit (widget.id,widget.nombre,widget.direccion,widget.telefono),
    );
  }
}

class Customedit extends StatefulWidget {

  var id;
  var nombre;
  var direccion;
  var telefono;

  Customedit(this.id,this.nombre,this.direccion,this.telefono);
  @override
  CustomeditState createState() {
    return CustomeditState();
  }
}
// Create a corresponding State class, which holds data related to the form.
class CustomeditState extends State<Customedit> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();
  String nombre    ;
  String direccion   ;
  String telefono  ;

  initState(){
    nombre ="${widget.nombre}";
    direccion="${widget.direccion}";
    telefono="${widget.telefono}";
  }

  _Editclient (BuildContext Context) async{
print(widget.id);
    http.post("${globals.constants.urlApi}api/clientes/saverecomendado",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "idrecomendado":"${widget.id}",
          "direccion": "${direccion}",
          "nombre": "${nombre}",
          "telefono": "${telefono}",
          "idusuario":"${globals.constants.iduser}"
        }
    ).then((http.Response response) {

      String res=response.body;
      var data = jsonDecode(response.body);
      print(data);
      print(res);
      if(data["res"]) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => misrecomendados(),
        ));
      }
      /* globals.constants.ejecuto=0;
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => misrecomendados(),
        ));
      }*/
    });
  }

  @override
  Widget build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return
      Form(
        key: _formKey,
        child:
        ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Colors.white,
                  height: 20,
                  thickness: 5,
                  indent: 20,
                  endIndent: 0,
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child:TextFormField(
                        initialValue: nombre,
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.person),
                          hintText: 'Nombre..',
                          labelText: "Nombre..",
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingresa un nombre valido';
                          }
                          return null;
                        },
                        onChanged: (value){
                          nombre=value;
                        },
                      )
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        initialValue: direccion,
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.apps),
                          hintText: 'Dirección...',
                          labelText: 'Dirección',
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingresa una dirección valida';
                          }
                          return null;
                        },
                        onChanged: (value){
                          direccion=value;
                        },
                      )
                  ),
                ) ,
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        initialValue: telefono,
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.email),
                          hintText: 'Telefono...',
                          labelText: 'Telefono',
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingresa un telefono valido';
                          }
                          return null;
                        },
                        onChanged: (value){
                          telefono=value;
                        },
                      )
                  ),
                ),
                new Container(
                    padding: const EdgeInsets.all(7.0),
                    child: Center(
                      child: new RaisedButton(
                        child: const Text('Guardar'),
                        shape:StadiumBorder(),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _Editclient(context);
                            Scaffold.of(context)
                                .showSnackBar(
                                SnackBar(content: Text('Guardando...')));

                          }
                        },
                      ),
                    )),
              ],
            ),
          ],
        ),
      );
  }
}




