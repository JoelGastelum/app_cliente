import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'constants.dart' as globals;
import 'package:expandable/expandable.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'facturas_externas.dart';
import 'main.dart';

String _rfcFiltro = "Contribuyentes";
String _Viajefiltro = "Viajes";
var _idsfacturas = [];
String correoDefault="";
String buscar='0';
class misfacturas extends StatefulWidget {
  @override
  misfacturasState createState() => misfacturasState();
}

class misfacturasState extends State<misfacturas> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: MenuLateral(),
      appBar: AppBar(
        actions: [
          RaisedButton(
            color: Colors.indigoAccent,
            onPressed: ()=>{Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => MandarFacturas(_idsfacturas,correoDefault),
            ))},
            child: Icon(Icons.share,color:Colors.white),
              /*Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => MandarFacturas(_idsfacturas),
              ))*/
          ),
        ],
        centerTitle: true,
        title: Text("Facturas"),
      ),
      body: listafacturas(),
      floatingActionButton:
      FloatingActionButton(
          onPressed: () => {
            if(_rfcFiltro!="Contribuyentes")
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) =>  facturas_externas("${_rfcFiltro}")))
            else
              _showAlertDialog("Seleccione un RFC","Aviso")
          },
          backgroundColor: Colors.blue,
          child: Icon(Icons.add)),
    );
  }
  void _showAlertDialog(String Mensaje, String Resultado) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text("${Resultado}"),
            content: Text("${Mensaje}"),
            actions: <Widget>[
              RaisedButton(
                color: Colors.red,
                child: Text(
                  "CERRAR",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop(MaterialPageRoute(
                    builder: (context) => Guest(),
                  ));
                },
              )
            ],
          );
        });
  }
}

class listafacturas extends StatefulWidget {
  @override
  listafacturasState createState() => listafacturasState();
}

class listafacturasState extends State<listafacturas> {
  List<String> RFCs = ['Contribuyentes'];
  List<String> Correos=[];
  List<String> Viajes = ['Viajes'];
  String rfc;
  String viaje;
  var presupuesto="0.00";
  var gastado="0.00";
  var faltante="0.00";
  var dateController = TextEditingController();
  var dateController2 = TextEditingController();
  var date = DateTime.now();

  @override
  void dispose() {
    // Clean up the controller when the widget is removed
    dateController2.dispose();
    super.dispose();
  }

  void initState() {
    super.initState();
    dateController =
        TextEditingController(text: "01/${date.month}/${date.year}");
    dateController2 =
        TextEditingController(text: "${date.day}/${date.month}/${date.year}");
    rfc = 'Contribuyentes';
    viaje = 'Viajes';
  }

  double _bodyHeight = 0.0;

  List<Widget> _facturas = [];


  Future<String> _GetFacturas() {
    _facturas.clear();
    _idsfacturas.clear();

    http.post("${globals.constants.urlApi}api/clientes/misfacturas",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "idcliente": "${globals.constants.iduser}",
          "nuevafecha": "${dateController.text}",
          "fecha": "${dateController2.text}",
          "rfc": "${_rfcFiltro}",
          "idviaje": "${_Viajefiltro}"
        }).then((http.Response response) {
      var data = jsonDecode(response.body);
        correoDefault=data['correo'];

      if(data['presupuesto']==null){
        data['presupuesto']=0.00;
      }
      if(data['gastado']==null){
        data['gastado']=0.00;
      }
      if(data['faltante']==null){
        data['faltante']=0.00;
      }
       presupuesto=data['presupuesto'];
      faltante=data['presupuesto'];
      gastado=data['gastado'];
       data=data['misfacturas'];
      print(data);
      //SE CREA UNA LISTA DE WIDGETS PARA INSERTARSE EN PANTALLA
      for (int k = 0; k < data.length; k++) {


        //PARA GENERAR UN QR SE HACE UN ARRAY QUE GUARDA LA POSICION
        if (!_idsfacturas.contains("${data[k]['id']}")) {
          _idsfacturas.insert(k, "${data[k]['id']}");
          _facturas.insert(
              k,
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                elevation: 10,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("${data[k]["nombrecomercial"]}",
                              ),
                          Text("Fecha: ${data[k]["fechatimbre"]}",
                          ),
                        ],
                      ),
                      subtitle:

                          Text("Total: ${data[k]["total"]}",
                          ),

                    ),
                  ],
                ),

              ));
        }
      }
    });
    buscar='1';
  }

  Duration get loginTime => Duration(milliseconds: 2250);
  Future<String> _calculation =
      Future.delayed(Duration(milliseconds: 5000)).then((_) {
    //SE HACE LA LLAMADA API
    return "lata ld";
  });

  @override
  Widget build(BuildContext context) {
    if (RFCs.length == 1) {
      http.post("${globals.constants.urlApi}api/user/clientes", headers: {
        "Accept": "application/json",
      }, body: {
        "idusuario": "${globals.constants.iduser}"
      }).then((http.Response response) {
        var datos = jsonDecode(response.body);
        var data = datos['clientes'];
        var viajesApi = datos['viajes'];
        for (int k = 0; k < viajesApi.length; k++) {
          if (!Viajes.contains(viajesApi[k]['descripcion'])) {
            Viajes.insert(k + 1, "${viajesApi[k]['descripcion']}");
          }
        }
        for (int k = 0; k < data.length; k++) {
          if (!RFCs.contains(data[k]['rfc'])) {
            RFCs.insert(k + 1, "${data[k]['rfc']}");
          }
        }
        setState(() {});
      });
    }
    _GetFacturas();
    return ListView(
      children: [
        ExpandableNotifier(
          initialExpanded: false,
          // <-- Provides ExpandableController to its children
          child: Column(
            children: [
              Expandable(
                // <-- Driven by ExpandableController from ExpandableNotifier
                collapsed: Container(
                  alignment: Alignment.centerRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.70,
                          height: MediaQuery.of(context).size.width * 0.13,
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          decoration: BoxDecoration(
                            color: Colors.lightBlue,
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: DropdownButton<String>(
                            value: rfc,
                            hint: Text(
                              "Contribuyentes",
                              style: TextStyle(color: Colors.black45),
                            ),
                            onChanged: (String data) {
                              setState(() {
                                rfc = data;
                                _rfcFiltro = data;
                              });
                            },
                            items: RFCs.map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child:
                                    SizedBox(child: Center(child: Text(value))),
                              );
                            }).toList(),
                            isExpanded: true,
                          ),
                        ),
                      ),
                      ExpandableButton(
                        // <-- Expands when tapped on the cover photo

                        child: Icon(
                          Icons.keyboard_arrow_down_rounded,
                          size: 50,
                        ),
                      ),
                    ],
                  ),
                ),
                expanded: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.70,
                            height: MediaQuery.of(context).size.width * 0.13,
                            padding: EdgeInsets.symmetric(horizontal: 10.0),
                            decoration: BoxDecoration(
                              color: Colors.lightBlue,
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: DropdownButton<String>(
                              value: rfc,
                              hint: Text(
                                "Contribuyentes",
                                style: TextStyle(color: Colors.black45),
                              ),
                              onChanged: (String data) {
                                setState(() {
                                  rfc = data;
                                  _rfcFiltro = data;
                                });
                              },
                              items: RFCs.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: SizedBox(
                                      child: Center(child: Text(value))),
                                );
                              }).toList(),
                              isExpanded: true,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: ExpandableButton(
                            // <-- Expands when tapped on the cover photo

                            child: Icon(
                              Icons.keyboard_arrow_up_rounded,
                              size: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.70,
                          height: MediaQuery.of(context).size.width * 0.13,
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          decoration: BoxDecoration(
                            color: Colors.lightBlue,
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: DropdownButton<String>(
                            value: viaje,
                            hint: Text(
                              "Viajes",
                              style: TextStyle(color: Colors.black),
                            ),
                            onChanged: (String data) {
                              setState(() {
                                viaje = data;
                                _Viajefiltro = data;
                                Timer.periodic(Duration(seconds: 1), (timer) {
                                  setState(() {
                                    presupuesto;
                                    _facturas;
                                    gastado;
                                    faltante;
                                  });
                                  timer.cancel();
                                });

                              });
                            },
                            items: Viajes.map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child:
                                    SizedBox(child: Center(child: Text(value))),
                              );
                            }).toList(),
                            isExpanded: true,
                          ),
                        ),
                      ),
                    ]),
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.45,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: TextField(
                              readOnly: true,
                              controller: dateController,
                              decoration: const InputDecoration(
                                prefixIcon: const Icon(Icons.calendar_today),
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              ),
                              onTap: () async {
                                var date = await showDatePicker(
                                    locale: Locale("es","ES"),
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(1900),
                                    lastDate: DateTime(2100));
                                dateController.text =
                                    "${date.day}/${date.month}/${date.year}";
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.45,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: TextField(
                              readOnly: true,
                              controller: dateController2,
                              decoration: const InputDecoration(
                                prefixIcon: const Icon(Icons.calendar_today),
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              ),
                              onTap: () async {
                                var date = await showDatePicker(
                                    locale: Locale("es","ES"),
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(1900),
                                    lastDate: DateTime(2100));
                                dateController2.text =
                                    "${date.day}/${date.month}/${date.year}";
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    Center(
                      child: Container(
                        width: 150,
                        child: RaisedButton(
                          child: const Text('Filtrar'),
                          shape: StadiumBorder(),
                          onPressed: () => {
                            _calculation,
                            _GetFacturas,
                            setState(() {
                              _facturas;
                              presupuesto;
                              gastado;
                              faltante;
                            })
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        RegistrosFacturas(_facturas, _idsfacturas,presupuesto,gastado,faltante)
      ],
    );
  }
}

class RegistrosFacturas extends StatefulWidget {
  var facturas;
  var _ids;
  var presupuesto;
  var gastado;
  var faltante;

  RegistrosFacturas(this.facturas, this._ids,this.presupuesto,this.gastado,this.faltante);

  @override
  RegistrosFacturasState createState() => RegistrosFacturasState();
}

class RegistrosFacturasState extends State<RegistrosFacturas> {
  buscarcambios() {
    Timer.periodic(Duration(seconds: 2), (timer) {
      print('buscar');
      setState(() {
        widget.facturas;
        widget.faltante;
        widget.gastado;
        widget.presupuesto;
        widget._ids;
      });
      timer.cancel();
    });

  }
  Future<String> _calculation =
      Future.delayed(Duration(milliseconds: 5000)).then((_) {
    //SE HACE LA LLAMADA API
    return "lata ld";
  });

  @override
  Widget build(BuildContext context) {
    if(buscar=="1"){
      buscarcambios();
      buscar='0';
    }

    // TODO: implement build
    return (FutureBuilder<String>(
      future: _calculation, // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        List<Widget> children;
        if (snapshot.hasData) {
          children = <Widget>[];
        } else if (snapshot.hasError) {
          children = <Widget>[
            Icon(
              Icons.error_outline,
              color: Colors.red,
              size: 60,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Text('Error: ${snapshot.error}'),
            )
          ];
        } else {
          return Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 50.0),
            child: SizedBox(
              child: CircularProgressIndicator(),
              width: 60,
              height: 60,
            ),
          ));
        }
        return Column(
          children: [
            if(widget.presupuesto!='0.00')
            Text("Presupuesto: ${widget.presupuesto}"),
            Text("Comprobado: ${widget.gastado}"),
            if(widget.faltante!='0.00')
            Text("Por comprobar: ${widget.faltante}"),
            ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: widget.facturas.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return InkWell(
                    child: widget.facturas[index],
                    onTap: () => [
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => WebViewFactura(widget._ids[index])))
                    ],
                  );
                }),
          ],
        );
      },
    ));
  }
}

class WebViewFactura extends StatefulWidget {
  var idfactura;

  WebViewFactura(this.idfactura);

  @override
  _WebViewFacturaState createState() => _WebViewFacturaState();
}

class _WebViewFacturaState extends State<WebViewFactura> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mi Factura'),
        actions: [
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                    builder: (context) => MandarCorreoXid(widget.idfactura))),
          )
        ],
        // This drop down menu demonstrates that Flutter widgets can be shown over the web view.
      ),
      // We're using a Builder here so we have a context that is below the Scaffold
      // to allow calling Scaffold.of(context) so we can show a snackbar.
      body: Builder(builder: (BuildContext context) {
        return WebView(
          initialUrl:
              'https://docs.google.com/viewerng/viewer?url=${globals.constants.urlApi}api/empresa/downloadpdfxid/${widget.idfactura}/${globals.constants.iduser}',
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          // TODO(iskakaushik): Remove this when collection literals makes it to stable.
          // ignore: prefer_collection_literals
          javascriptChannels: <JavascriptChannel>[
            _toasterJavascriptChannel(context),
          ].toSet(),
          gestureRecognizers: Set()
            ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()
              ..onTapDown = (tap) {
                print("This one prints");
              })),
          onPageStarted: (String url) {
            print('Page started loading: $url');
          },
          onPageFinished: (String url) {
            print('Page finished loading: $url');
          },
          gestureNavigationEnabled: true,
        );
      }),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }
}

enum MenuOptions {
  showUserAgent,
  listCookies,
  clearCookies,
  addToCache,
  listCache,
  clearCache,
  navigationDelegate,
}

class MandarCorreoXid extends StatelessWidget {
  var idcomprobante;

  MandarCorreoXid(this.idcomprobante);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
            title: Text("Compartir"),
            leading: IconButton(
                icon: Icon(Icons.chevron_left),
                onPressed: () => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => misfacturas())))),
        body: Center(
          child: Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [SendCorreoXid(this.idcomprobante)]),
          ),
        ),
    );
  }
}

class SendCorreoXid extends StatefulWidget {
  var idcomprobante;

  SendCorreoXid(this.idcomprobante);

  @override
  SendCorreoStateXid createState() {
    return SendCorreoStateXid();
  }
}

// Create a corresponding State class, which holds data related to the form.
class SendCorreoStateXid extends State<SendCorreoXid> {
  final _formKey = GlobalKey<FormState>();
  String email;

  Future<String> _EnviarCorreo() {
    var data;
    http.post("${globals.constants.urlApi}api/empresa/enviarfactura",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "idcomprobante": "${widget.idcomprobante}",
          "correo": "${email}"
        }).then((http.Response response) {
      String res = response.body;
      data = jsonDecode(response.body);
      print(data);
      if (!data['res']) {
        _showAlertDialog(data['msg'], "Error");
      } else {
        _showAlertDialog(data['msg'], "Enviado!");
      }
    });
  }

  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
    () => 'Data Loaded',
  );

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return FutureBuilder<String>(
        future: _calculation, // a previously-obtained Future<String> or null
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          List<Widget> children;
          if (snapshot.hasData) {
            children = <Widget>[];
          } else if (snapshot.hasError) {
            children = <Widget>[
              Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Error: ${snapshot.error}'),
              )
            ];
          } else {
            return Container(
              color: Colors.blue,
              padding: EdgeInsets.only(top: 200),
              child: Center(
                  child: SizedBox(
                child: CircularProgressIndicator(),
                width: 60,
                height: 60,
              )),
            );
          }
          return Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.attach_email),
                          hintText: 'Email...',
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                        ),
                        onChanged: (text) {
                          email = text;
                        },
                      )),
                ),
                Container(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: new RaisedButton(
                        child: const Text('Enviar'),
                        shape: StadiumBorder(),
                        onPressed: () {
                          _EnviarCorreo();
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('Enviando...')));
                        },
                      ),
                    )),
              ],
            ),
          );
        });
  }

  void _showAlertDialog(String Mensaje, String Resultado) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text("${Resultado}"),
            content: Text("${Mensaje}"),
            actions: <Widget>[
              RaisedButton(
                color: Colors.red,
                child: Text(
                  "CERRAR",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop(MaterialPageRoute(
                    builder: (context) => Guest(),
                  ));
                },
              )
            ],
          );
        });
  }
}

class MandarFacturas extends StatelessWidget {
  var _facturasmail;
  var correoDefault;
  MandarFacturas(this._facturasmail,this.correoDefault);
  @override
  Widget build(BuildContext context) {
    print(_facturasmail);
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.blue[255],
        appBar: AppBar(
            title: Text("Compartir"),
            leading: IconButton(
                icon: Icon(Icons.chevron_left),
                onPressed: () => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => misfacturas())))),
        body: Center(
          child: Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [SendFacturas(this._facturasmail)]),
          ),
        ));
  }
}


class SendFacturas extends StatefulWidget {
var _facturasEmail;
SendFacturas(this._facturasEmail);
  @override
  SendFacturasState createState() {
    return SendFacturasState();
  }
}
// Create a corresponding State class, which holds data related to the form.
class SendFacturasState extends State<SendFacturas> {
  final _formKey = GlobalKey<FormState>();
  String email;
  Future<String> _EnviarCorreo(){
    var data;
    print(widget._facturasEmail);
    http.post("${globals.constants.urlApi}api/clientes/enviarfacturas",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "idusuario": "${globals.constants.iduser}",
          "idfacturas" : "${widget._facturasEmail}",
          "correo":"${email}"
        }
    ).then((http.Response response) {
      String res=response.body;

      data = jsonDecode(response.body);
      print(data);
      if(data['res']==null){
        data['res']=false;
      }
      if(data['res']){
        _showAlertDialog(data['msg'],"Guardado!");
        print(1);
      }else{
        _showAlertDialog("Error al Enviar","Error");
        print(0);
      }
    });
  }
  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
        () => 'Data Loaded',);

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
          return Form(
            key: _formKey,
            child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        initialValue: correoDefault,
                        maxLines: 3,
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.attach_email),
                          hintText: 'Email...',
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                        ),
                        onChanged: (text) {
                          email = text;
                        },
                      )
                  ),
                ),
                Container(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: new RaisedButton(
                        child: const Text('Enviar'),
                        shape: StadiumBorder(),
                        onPressed: () {
                          _EnviarCorreo();

                        },
                      ),
                    )),
              ],
            ),
          );
  }
  void _showAlertDialog(String Mensaje,String Resultado) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text("${Resultado}"),
            content: Text("${Mensaje}"),
            actions: <Widget>[
              RaisedButton(
                color: Colors.red,
                child: Text("CERRAR", style: TextStyle(color: Colors.white),),
                onPressed: (){      Navigator.of(context).pop(MaterialPageRoute(
                  builder: (context) => Guest(),
                ));
                },
              )
            ],
          );
        });
  }
}
