import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_new/show_alert_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:flutter/foundation.dart';

import 'facturas_externas.dart';
import 'main.dart';
import 'misrecomendados.dart';

class constants {
  static var iduser;
  static var nombreuser;
  static var idcliente;
  static var TokenLogin = '0';
  static var ejecuto = 0;
  static var urlApi="https://mefactura.com/";
}

void showAlertDialog(BuildContext context, String index, String hexa) {
  const platform = const MethodChannel('sendSms');
  List<String> people = [];
  String numero;
  var continuar = 0;
  buscarPDF(BuildContext context) {
    Timer.periodic(Duration(seconds: 6), (timer) {
      if (continuar == 1) {
        timer.cancel();
      }
      http
          .get(
              "${constants.urlApi}api/empresa/listopdf/${constants.iduser}")
          .then((http.Response response) {
        var data = jsonDecode(response.body);
        if (data["res"]) {
          timer.cancel();
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => WebViewExample()));
        }
      });
    });
  }

  Future<Null> sendSms2() async {
    print("SendSMS");
    try {
      final String result = await platform.invokeMethod(
          'send', <String, dynamic>{
        "phone": "+52" + numero,
        "msg": index
      }); //Replace a 'X' with 10 digit phone number
      print(result);
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }

  showDialog(
    context: context,
    builder: (BuildContext context) {
      buscarPDF(context);
      return CustomAlertDialog(
        actions: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(right: 8.0, bottom: 20.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: RaisedButton(
                  shape: StadiumBorder(),
                  child: Text("SMS"),
                  color: Colors.green,
                  onPressed: () => {sendSms2(), continuar = 1},
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 20.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: RaisedButton(
                  child: Text("Cerrar"),
                  shape: StadiumBorder(),
                  color: Colors.redAccent,
                  onPressed: () =>
                      {continuar = 1, Navigator.pop(context), people.clear()},
                ),
              ),
            ],
          ),
        ],
        content: Container(
            width: MediaQuery.of(context).size.width / .9,
            height: MediaQuery.of(context).size.height / 2.3,
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color: const Color(0xFFFFFF),
              borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
            ),
            child: ListView(
              children: [
                Center(
                    child: QrImage(
                  data: index,
                  size: 175,
                )),
                Center(child: Text("Su codigo es: ${hexa}")),
                Center(child: Text("No cerrar QR hasta ver su factura")),
                Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.phone_android),
                        hintText: 'Numero de celular...',
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                      ),
                      onChanged: (text) {
                        numero = text;
                      },
                    ))
              ],
            ) //Contents here
            ),
      );
    },
  );
}

void showContribuyente(BuildContext context, String index) {
  const platform = const MethodChannel('sendSms');
  List<String> people = [];
  String numero;
  String codigo = constants.iduser.toRadixString(16).padLeft(5, '0');
  Future<Null> sendSms2() async {
    print("SendSMS");
    try {
      final String result =
          await platform.invokeMethod('send', <String, dynamic>{
        "phone": "+52" + numero,
        "msg":
            "Tu código es ${index} ${constants.urlApi}guardar/${index}"
      }); //Replace a 'X' with 10 digit phone number
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => misrecomendados()));
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return CustomAlertDialog(
        actions: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(right: 8.0, bottom: 20.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: RaisedButton(
                  shape: StadiumBorder(),
                  child: Icon(Icons.copy_outlined),
                  color: Colors.grey,
                  onPressed: () {
                    Clipboard.setData(ClipboardData(text: "${codigo}"));
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 8.0, bottom: 20.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: RaisedButton(
                  shape: StadiumBorder(),
                  child: Text("SMS"),
                  color: Colors.green,
                  onPressed: () => {sendSms2()},
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 20.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: RaisedButton(
                  child: Text("Cerrar"),
                  shape: StadiumBorder(),
                  color: Colors.redAccent,
                  onPressed: () => {Navigator.pop(context), people.clear()},
                ),
              ),
            ],
          ),
        ],
        content: Container(
            width: MediaQuery.of(context).size.width / .9,
            height: MediaQuery.of(context).size.height / 2,
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color: const Color(0xFFFFFF),
              borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
            ),
            child: ListView(
              children: [
                QrImage(
                    data: "${constants.urlApi}guardar/${index}"),
                Center(child: Text("Tu código es: ${codigo}")),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.phone_android),
                          hintText: 'Numero de celular...',
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                        ),
                        onChanged: (text) {
                          numero = text;
                        },
                      )),
                )
              ],
            ) //Contents here
            ),
      );
    },
  );
}

void saveValue(String token) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('token', token);
  constants.TokenLogin = token;
}

void Logout(BuildContext Context) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  //Remove String
  prefs.remove("token");
  constants.ejecuto = 0;
  constants.TokenLogin = '0';
  constants.iduser = 0;
  constants.idcliente;

  //Remove bool
}
