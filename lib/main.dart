import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:http/http.dart' as http;
import 'constants.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';
import 'misrecomendados.dart';
import 'newcliente.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'misfacturas.dart';
import 'misviajes.dart';



List<Widget> _widgets =[] ;
var _ids =[] ;
var _hexa=[];

//FUNCION QUE SE ENCARGA QUE EL USUARIO NO TENGA QUE LOGEARSE CADA QUE ENTRa
getToken() async {

  _widgets.clear();
  _ids.clear();
  _hexa.clear();
  //SE VERIFICA QUE EL TOKEN ESTE GUARDADO EN MEMORIA
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');
//SI HAY UN TOKEN GUARDADO EMPIEZA EL PROCESO
  if(token!=null){
    String res;
    //SE HACE UNA LLAMADA POST DONDE LA API RECIBE EL TOKEN
    http.post("${globals.constants.urlApi}api/user/loginwithtoken",
        headers: {
          "Accept": "application/json",
        },
        body: {"tokenLogin": token}
    ).then((http.Response response) {
      res = response.body;

      var data = jsonDecode(response.body);
      //SE PONEN LAS VARIABLES GLOBALES PARA USARSE MAS ADELANTE
      if(data["message"]!=null){
        globals.constants.TokenLogin='0';

      }else{
        globals.constants.iduser = data["id"];
        globals.constants.nombreuser=data['nombre'];
        globals.constants.TokenLogin = data["tokenlogin"];
      }
    });
  }
  return token;
}
void main(){
  //SE INICIALIZAN LOS WIDGETS

  WidgetsFlutterBinding.ensureInitialized();
  getToken();
//ESPERA A QUE LA FUNCION GET TOKEN TERMINE PARA PREPARAR EL INICIO DEL PROGRAMA
  Future.delayed(Duration(milliseconds: 2250)).then((_){
    runApp(MyApp());
  });

}

class MyApp extends StatelessWidget {
  @override


  Widget build(BuildContext context) {
//SI IDS TIENE DATOS ENTONCES SE SALTA EL LOGIN
if(globals.constants.TokenLogin=='0'){
        return MaterialApp(
          title: 'Login Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue[255],
            textTheme: TextTheme(
              display2: TextStyle(
                fontFamily: 'OpenSans',
                fontSize: 45.0,
              ),
              button: TextStyle(
                fontFamily: 'OpenSans',
              ),
            ),
          ),
          home: LoginScreen(),
        );
      }else{
  return MaterialApp(
    localizationsDelegates: [
      GlobalMaterialLocalizations.delegate
    ],
    supportedLocales: [
      const Locale('en'),
      const Locale('es')
    ],
    title: 'Login Demo',
    theme: ThemeData(
      buttonColor: Colors.blue,
      primarySwatch: Colors.blue[255],
      secondaryHeaderColor: Colors.blue,
      textTheme: TextTheme(
        display2: TextStyle(
          fontFamily: 'OpenSans',
          fontSize: 45.0,
          color:Colors.blue
        ),
        button: TextStyle(
          fontFamily: 'OpenSans',
          color:Colors.blue
        ),
      ),
    ),
    home: Guest(),
  );

      }
    }
  }
//PANTALLA DE LOGEO
class LoginScreen extends StatelessWidget {
  Duration get loginTime => Duration(milliseconds: 2250);
  Future<String> _authUser(LoginData data) {
    String res;
    //SE HACE LA LLAMADA API
    http.post("${globals.constants.urlApi}api/user/login",
        headers: {
          "Accept": "application/json",
        },
        body: {"login": "${data.name}", "pwd": "${data.password}"}
    ).then((http.Response response) {
      res = response.body;
      var data = jsonDecode(response.body);
      print(data);
      globals.constants.iduser = data["id"];
      globals.constants.nombreuser = data["nombre"];
      globals.constants.TokenLogin = data["tokenlogin"];
      globals.saveValue(data["tokenlogin"]);
    });

    return Future.delayed(loginTime).then((_) {
      if(res!=""){
        return null;
      }else
        return "Credenciales invalidas";
    });

  }


  Future<String> _recoverPassword(String name) async{

    return Future.delayed(loginTime).then((_) {

      return null;
    });
  }
  @override
  Widget build(BuildContext context) {

    return FlutterLogin(
      title: 'MeFactura',
      logo: '',
      onLogin: _authUser,
      onSignup: _authUser,
      onSubmitAnimationCompleted: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Guest(),
        ));
      },
      onRecoverPassword: _recoverPassword,
      messages: LoginMessages(
        usernameHint: 'Usuario',
        passwordHint: 'Contraseña',
        confirmPasswordHint: 'Confirmar',
        loginButton: 'Iniciar Sesion',
        signupButton: 'Registrarse',
        forgotPasswordButton: 'Olvidaste la contraseña?',
        recoverPasswordButton: 'Ayuda',
        goBackButton: 'Atras',
        confirmPasswordError: 'El usuario o contraseñas no coinciden!',
        recoverPasswordDescription:
        '',
        recoverPasswordSuccess: 'Password rescued successfully',
      ),
    );
  }
}


class Guest extends StatefulWidget {

  @override
  State<StatefulWidget> createState()=> _State() ;

}


class _State extends State<Guest> {

  Widget build(BuildContext context) {
    if(globals.constants.ejecuto==0){
      _widgets.clear();
      _ids.clear();
      _hexa.clear();

      http.post("${globals.constants.urlApi}api/user/clientes",
          headers: {
            "Accept": "application/json",
          },
          body: {"idusuario": "${globals.constants.iduser}"}
      ).then((http.Response response) {

        var dato = jsonDecode(response.body);
          var data=dato['clientes'];
        //SE CREA UNA LISTA DE WIDGETS PARA INSERTARSE EN PANTALLA
        for (int k = 0; k < data.length; k++) {
          //PARA GENERAR UN QR SE HACE UN ARRAY QUE GUARDA LA POSICION
          if (!_ids.contains("${globals.constants.urlApi}consultaqr/${globals.constants.TokenLogin}/${data[k]["rfc"]}")) {
            _ids.insert(k,
                "${globals.constants.urlApi}consultaqr/${globals.constants.TokenLogin}/${data[k]["rfc"]}");
            _hexa.insert(k,
                "${data[k]["hexa"]}");
            _widgets.insert(k,
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),

                  elevation: 10,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        leading: CircleAvatar(
                          backgroundImage: AssetImage(
                              "assets/images/qrcode.png"),
                          backgroundColor: Colors.white,
                          radius: 40,)
                        ,
                        title: Text("${data[k]["rfc"]}",
                           ),
                        subtitle: Text("${data[k]["nombre"]}",
                            ),
                        enabled: false,
                      ),
                      ButtonTheme.bar(
                        child: ButtonBar(
                          children: <Widget>[
                            RaisedButton(
                                child: const Text('Editar',
                                  ),
                                onPressed: () =>
                                    Navigator.push(context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditClient(data[k]))),
                                color: Colors.blue[0]
                            ),
                            RaisedButton(
                                child: const Text('Eliminar',
                                    style: TextStyle(color: Colors.white)),
                                onPressed: () =>
                                    showAlert(
                                        context, "${data[k]["id"]}",
                                        "${data[k]["rfc"]}", k),
                                color: Colors.red
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
            );
          }
        }
      });
      globals.constants.ejecuto = 1;
    }
    return Scaffold(
      drawer:MenuLateral(),
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed:()=>{
              globals.Logout(context),
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => LoginScreen(),
              )
              )
            },
          ),
        ],
        centerTitle: true,
          title: Text("TEST"),
      ),
      body: _registros(),
          floatingActionButton: FloatingActionButton(
          onPressed: ()=>  Navigator.push(context, MaterialPageRoute(builder: (context)=>NewClient())),
          backgroundColor: Colors.blue[255],
          child: Icon(Icons.add)
          ),
    );
  }
}


class MenuLateral extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: Text("Fe soluciones"),
            accountEmail: Text("contacto@fesoluciones.com"),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("https://ichef.bbci.co.uk/news/660/cpsprodpb/6AFE/production/_102809372_machu.jpg"),
                    fit: BoxFit.cover
                )
            ),
          ),
           Ink(
             decoration: BoxDecoration(boxShadow:[
               BoxShadow(
                 color: Colors.white.withOpacity(0.5),
                 spreadRadius: 5,
                 blurRadius: 7,
                 offset: Offset(0, 3), // changes position of shadow
               ),
             ],),
             child: new ListTile(
                title: Text("Mis Contribuyentes", style: TextStyle(color: Colors.black),),
               onTap: ()=>{Navigator.of(context).pushReplacement(MaterialPageRoute(
               builder: (context) => Guest()))},
              ),
           ),
          new ListTile(
            title: Text("Gana con nosotros!", style: TextStyle(color: Colors.black)),
              onTap: ()=>{Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => misrecomendados()))}
          ),
             new ListTile(
              title: Text("Mis Facturas", style: TextStyle(color: Colors.black)),
               onTap: ()=>Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>misfacturas())),
            ),
          new ListTile(
            title: Text("Mis Viajes", style: TextStyle(color: Colors.black)),
            onTap: ()=>Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>misviajes())),
          ),
         new ListTile(
              title: Text("Invitar Amigos", style: TextStyle(color: Colors.black)),
             onTap: ()=>{globals.showContribuyente(context, "${globals.constants.iduser}")},
            ),
        ],
      ) ,
    );
  }
}


class _registros extends StatelessWidget {
  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 5),
        () => 'Data Loaded',
  );
  @override
  Widget build(BuildContext context) {

    return (
        FutureBuilder<String>(
          future: _calculation, // a previously-obtained Future<String> or null
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            List<Widget> children;
            if (snapshot.hasData) {
              children = <Widget>[
              ];
            } else if (snapshot.hasError) {
              children = <Widget>[
                Icon(
                  Icons.error_outline,
                  color: Colors.red,
                  size: 60,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Text('Error: ${snapshot.error}'),
                )
              ];
            } else {
              return
                Center(
                child:SizedBox(
                  child: CircularProgressIndicator(),
                  width: 60,
                  height: 60,
                  )
                );
            }
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text("Bienvenido ${globals.constants.nombreuser}"),
                ),
                ListView.builder
                  (
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: _widgets.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return InkWell(
                        child: _widgets[index],
                        onTap: () =>
                           [ globals.showAlertDialog(context, _ids[index],_hexa[index])],

                      );
                    }
                ),
              ],
            );
          },
        )
    );
  }
}
showAlert(BuildContext context,String index,String rfc,int pos) {

  _Deleteclient (BuildContext Context) async{
    http.post("${globals.constants.urlApi}api/clientes/delete",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "idcliente": "${index}",
          "idusuario":"${globals.constants.iduser}"
        }
    ).then((http.Response response) {
      _widgets.clear();
      _ids.clear();
      _hexa.clear();
      String res=response.body;
      var data = jsonDecode(response.body);
      if(data["res"]){
        globals.constants.ejecuto=0;
        Navigator.push(context, MaterialPageRoute(builder: (context)=>Guest()));

      }
    });
  }

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('${rfc}'),
        content: Text("Seguro desea eliminar contribuyente?"),
        actions: <Widget>[
          FlatButton(
            child: Text("Si"),
            onPressed: () {
              _Deleteclient(context);
              globals.constants.ejecuto=0;
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text("No"),
            onPressed: () {
              //Put your code here which you want to execute on No button click.
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text("Cancelar"),
            onPressed: () {
              //Put your code here which you want to execute on Cancel button click.
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

class WebViewExample extends StatefulWidget {
  @override
  _WebViewExampleState createState() => _WebViewExampleState();
}

class _WebViewExampleState extends State<WebViewExample> {
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Me Facturas'),
        actions: [
          IconButton(
            icon:Icon(Icons.share),
            onPressed:()=>Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => MandarCorreo())),
          )
        ],
        // This drop down menu demonstrates that Flutter widgets can be shown over the web view.
      ),
      // We're using a Builder here so we have a context that is below the Scaffold
      // to allow calling Scaffold.of(context) so we can show a snackbar.
      body: Builder(builder: (BuildContext context) {
        return WebView(
          initialUrl: 'https://docs.google.com/viewerng/viewer?url=${globals.constants.urlApi}empresa/descargarpdf/${globals.constants.iduser}',
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          // TODO(iskakaushik): Remove this when collection literals makes it to stable.
          // ignore: prefer_collection_literals
          javascriptChannels: <JavascriptChannel>[
            _toasterJavascriptChannel(context),
          ].toSet(),
          gestureRecognizers: Set()
            ..add(
                Factory<TapGestureRecognizer>(() => TapGestureRecognizer()
                  ..onTapDown = (tap) {
                    print("This one prints");
                  })),
          onPageStarted: (String url) {
            print('Page started loading: $url');
          },
          onPageFinished: (String url) {
            print('Page finished loading: $url');
          },
          gestureNavigationEnabled: true,
        );
      }),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }
}
enum MenuOptions {
  showUserAgent,
  listCookies,
  clearCookies,
  addToCache,
  listCache,
  clearCache,
  navigationDelegate,
}


class MandarCorreo extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.blue,
        appBar:AppBar(
            title:Text("Compartir"),
            leading:IconButton(
                icon:Icon(Icons.chevron_left),
                onPressed:()=>Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => Guest()))
            )
        ),
        body:Center(
          child: Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children:[
                  SendCorreo()
                ]
            ),
          ),
        )

    );
  }
}



class SendCorreo extends StatefulWidget {

  @override
  SendCorreoState createState() {
    return SendCorreoState();
  }
}
// Create a corresponding State class, which holds data related to the form.
class SendCorreoState extends State<SendCorreo> {
  final _formKey = GlobalKey<FormState>();
  String email;
  Future<String> _EnviarCorreo(){
    var data;
    http.post("${globals.constants.urlApi}api/empresa/enviarfactura",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "idcliente": "${globals.constants.iduser}",
          "correo": "${email}"

        }
    ).then((http.Response response) {
      String res=response.body;
      data = jsonDecode(response.body);
      print(res);
      if(!data['res']){
        _showAlertDialog(data['msg'],"Error");
      }else{
        _showAlertDialog(data['msg'],"Guardado!");
      }
    });
  }

  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
        () => 'Data Loaded',);

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.



  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return FutureBuilder<String>(
        future: _calculation, // a previously-obtained Future<String> or null
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          List<Widget> children;
          if (snapshot.hasData) {
            children = <Widget>[
            ];
          } else if (snapshot.hasError) {
            children = <Widget>[
              Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Error: ${snapshot.error}'),
              )
            ];
          } else {
            return
              Container(
                color:Colors.blue,
                padding:EdgeInsets.only(top:200),
                child: Center(
                    child: SizedBox(
                      child: CircularProgressIndicator(),
                      width: 60,
                      height: 60,
                    )
                ),
              );
          }
          return Form(
            key: _formKey,
            child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Colors.blue,
                  height: 20,
                  thickness: 5,
                  indent: 20,
                  endIndent: 0,
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                      decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.attach_email),
                          hintText: 'Email...',
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                        ),
                        onChanged: (text) {
                          email = text;
                        },
                      )
                  ),
                ),
                Container(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: new RaisedButton(
                        child: const Text('Enviar'),
                        shape: StadiumBorder(),
                        color: Colors.deepOrange,
                        onPressed: () {
                          _EnviarCorreo();
                          Scaffold.of(context)
                              .showSnackBar(
                              SnackBar(content: Text('Enviando...')));
                        },
                      ),
                    )),
              ],
            ),
          );
        });
  }
  void _showAlertDialog(String Mensaje,String Resultado) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text("${Resultado}"),
            content: Text("${Mensaje}"),
            actions: <Widget>[
              RaisedButton(
                color: Colors.red,
                child: Text("CERRAR", style: TextStyle(color: Colors.white),),
                onPressed: (){      Navigator.of(context).pop(MaterialPageRoute(
                  builder: (context) => Guest(),
                ));
                },
              )
            ],
          );
        });
  }
}
