import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'constants.dart' as globals;
import 'main.dart';

class NewClient extends StatefulWidget{

  @override
  State<StatefulWidget> createState()=> _StateClient() ;
}

class _StateClient extends State<NewClient> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Nuevo"),

      ),
      body:
      Center(
          child:ListView(
              children:[MyCustomForm()]
          )
      ),
    );
  }
}


class MyCustomForm extends StatefulWidget {

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}
// Create a corresponding State class, which holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();
  String nombre="";
  String rfc="";
  String email="";
  String calle="";
  String localidad="";

  _newclient (BuildContext Context) async{
    http.post("${globals.constants.urlApi}api/clientes/save",
        headers: {
          "Accept": "application/json",
        },
        body: {"rfc": "${rfc}",
          "nombre": "${nombre}",
          "correo": "${email}",
          "calle": "${calle}",
          "localidad": "${localidad}",
          "idusuario":"${globals.constants.iduser}"
        }
    ).then((http.Response response) {
      var data = jsonDecode(response.body);
      if(data["res"]){
        globals.constants.ejecuto=0;
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Guest(),
        ));
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return
      Form(
        key: _formKey,
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: <Widget>[
            Divider(
              color: Colors.white,
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 0,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child:TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: const Icon(Icons.person),
                      hintText: 'Nombre..',
                      labelText:'Nombre',
                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un nombre valido';
                      }
                      return null;
                    },
                    onChanged: (text){
                      nombre=text;
                    },
                  )
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child: TextFormField(
                    decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.apps),
                        hintText: 'RFC...',
                        labelText: 'RFC',
                        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un rfc valido';
                      }
                      return null;
                    },
                    onChanged: (text){
                      rfc=text;
                    },
                  )
              ),
            ) ,
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: const Icon(Icons.email),
                      hintText: 'Correo...',
                      labelText: 'Correo',
                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un correo valido';
                      }
                      return null;
                    },
                    onChanged: (text){
                      email=text;
                    },
                  )
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child:  TextFormField(
                    decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.add_location_alt),
                        hintText: 'Calle...',
                        labelText: 'Calle',
                        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa una calle valida';
                      }
                      return null;
                    },
                    onChanged: (text){
                      calle=text;
                    },
                  )
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child:   TextFormField(
                    decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.wine_bar_rounded),
                        hintText: 'Localidad...',
                        labelText: 'Localidad',
                        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa una localidad valida';
                      }
                      return null;
                    },
                    onChanged: (text){
                      localidad=text;
                    },
                  )
              ),
            ),
            new Container(
                padding: const EdgeInsets.all(20.0),
                child: Center(
                  child: new RaisedButton(
                    child: const Text('Guardar'),
                    color: Colors.deepOrange,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _newclient(context);
                        globals.constants.ejecuto=0;
                        Scaffold.of(context)
                            .showSnackBar(SnackBar(content: Text('Guardando...')));
                      }

                    },
                  ),
                )),
          ],
        ),
      );
  }
}



class EditClient extends StatefulWidget{
  var data;
  EditClient(this.data);
  @override
  State<StatefulWidget> createState()=> _StateClientEdit() ;
}

class _StateClientEdit extends State<EditClient> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Nuevo"),

      ),
      body:
      Center(
          child:ListView(
              children:[MyCustomFormEdit(this.widget.data)]
          )
      ),
    );
  }
}
class MyCustomFormEdit extends StatefulWidget {
  var data;
  MyCustomFormEdit(this.data);
  @override
  MyCustomFormStateEdit createState() {
    return MyCustomFormStateEdit();
  }
}
// Create a corresponding State class, which holds data related to the form.
class MyCustomFormStateEdit extends State<MyCustomFormEdit> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.


  final _formKey = GlobalKey<FormState>();
  TextEditingController nombre;
  TextEditingController rfc   ;
  TextEditingController email ;
  TextEditingController calle ;
  TextEditingController localidad;


  void initState() {
    super.initState();
    nombre = TextEditingController(text: "${widget.data['nombre']}");
    rfc = TextEditingController(text: "${widget.data['rfc']}");
    email = TextEditingController(text: "${widget.data['correo']}");
    calle = TextEditingController(text: "${widget.data['calle']}");
    localidad = TextEditingController(text: "${widget.data['localidad']}");

  }
  _Editclient (BuildContext Context) async{

    http.post("${globals.constants.urlApi}api/clientes/edit",
        headers: {
          "Accept": "application/json",
        },
        body: {
      "rfc": "${rfc.text}",
          "nombre": "${nombre.text}",
          "correo": "${email.text}",
          "calle": "${calle.text}",
          "localidad": "${localidad.text}",
          "idcliente":"${widget.data['id']}",
          "idusuario":"${globals.constants.iduser}"
        }
    ).then((http.Response response) {

      String res=response.body;
      var data = jsonDecode(response.body);
      print(data);
      print(res);
      if(data["res"]){
        globals.constants.ejecuto=0;
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Guest(),
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return
      Form(
        key: _formKey,
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: <Widget>[
            Divider(
              color: Colors.white,
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 0,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child:TextFormField(
                    controller: nombre,
                    decoration: const InputDecoration(
                      prefixIcon: const Icon(Icons.person),
                      hintText: 'Nombre..',
                      labelText: "Nombre..",
                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un nombre valido';
                      }
                      return null;
                    },
                  )
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child: TextFormField(
                    controller:rfc,
                    decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.apps),
                        hintText: 'RFC...',
                        labelText: 'RFC',
                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un rfc valido';
                      }
                      return null;
                    },
                  )
              ),
            ) ,
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child: TextFormField(
                    controller:email,
                    decoration: const InputDecoration(
                      prefixIcon: const Icon(Icons.email),
                      hintText: 'Correo...',
                      labelText: 'Correo',
                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un correo valido';
                      }
                      return null;
                    },
                  )
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child:  TextFormField(
                    controller:calle,
                    decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.add_location_alt),
                        hintText: 'Calle...',
                        labelText: 'Calle',
                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa una calle valida';
                      }
                      return null;
                    },
                  )
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius:BorderRadius.all(Radius.circular(20))),
                  child:   TextFormField(
                    controller:localidad,
                    decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.wine_bar_rounded),
                        hintText: 'Localidad...',
                        labelText: 'Localidad',
                        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa una localidad valida';
                      }
                      return null;
                    },
                  )
              ),
            ),
            new Container(
                padding: const EdgeInsets.all(20.0),
                child: Center(
                  child: new RaisedButton(
                    child: const Text('Guardar'),
                    color: Colors.deepOrange,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _Editclient(context);
                        globals.constants.ejecuto=0;
                        Scaffold.of(context)
                            .showSnackBar(
                            SnackBar(content: Text('Guardando...')));
                      }
                    },
                  ),
                )),
          ],
        ),
      );
  }
}



