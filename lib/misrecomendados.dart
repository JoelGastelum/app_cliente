import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_new/newrecomendado.dart';
import 'package:http/http.dart' as http;
import 'constants.dart' as globals;
import 'package:expandable/expandable.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'main.dart';

String buscar = "0";

class misrecomendados extends StatefulWidget {
  @override
  misrecomendadosState createState() => misrecomendadosState();
}

class misrecomendadosState extends State<misrecomendados> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: MenuLateral(),
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () => {
              globals.Logout(context),
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ))
            },
          ),
        ],
        centerTitle: true,
        title: Text("Recomendados"),
      ),
      body: listarecomendados(),
      floatingActionButton: FloatingActionButton(
          onPressed: () => {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => newrecomendado()))
              },
          backgroundColor: Colors.blue,
          child: Icon(Icons.add)),
    );
  }
}

class recomendados {
  String nombre;
  String estatus;
  String actualizacion;
  String comision;
  String direccion;
  String telefono;
  var id;

  recomendados(this.nombre, this.estatus, this.actualizacion, this.comision,
      this.direccion, this.telefono, this.id);
}

class listarecomendados extends StatefulWidget {
  @override
  listarecomendadosState createState() => listarecomendadosState();
}

class listarecomendadosState extends State<listarecomendados> {
  Future<String> _calculation =
      Future.delayed(Duration(milliseconds: 5000)).then((_) {
    //SE HACE LA LLAMADA API
    return "lata ld";
  });
  var dateController = TextEditingController();
  var dateController2 = TextEditingController();
  var date = DateTime.now();

  void initState() {
    super.initState();
    dateController =
        TextEditingController(text: "01/${date.month}/${date.year}");
    dateController2 =
        TextEditingController(text: "${date.day}/${date.month}/${date.year}");
  }

  List<recomendados> _recomendados = [];
  var idrecomendado;
  List<String> estatusAr = [
    'Todos',
    'Recomendados',
    'Seguimiento',
    'Cierre',
    'Declinada'
  ];
  String _estatus = "Todos";
  String _estatusFiltro = "Todos";

  Future<String> _GetFacturas() {
    _recomendados.clear();

    http.post("${globals.constants.urlApi}api/clientes/misrecomendados",
        headers: {
          "Accept": "application/json",
        },
        body: {
          "idusuario": "${globals.constants.iduser}",
          "fechainicio": "${dateController.text}",
          "fechafin": "${dateController2.text}",
          "estatus": "${_estatusFiltro}"
        }).then((http.Response response) {
      var data = jsonDecode(response.body);
      print(data);
      //SE CREA UNA LISTA DE WIDGETS PARA INSERTARSE EN PANTALLA
      for (int k = 0; k < data.length; k++) {
        _recomendados.insert(
            k,
            recomendados(
                data[k]['nombre'],
                data[k]['estatus'],
                data[k]['fecha'],
                "${data[k]['comision']}",
                "${data[k]['direccion']}",
                "${data[k]['telefono']}",
                "${data[k]['id']}"));
      }
    });

    setState(() {
      _recomendados;
    });

    buscar = '1';
  }

  @override
  Widget build(BuildContext context) {
    _GetFacturas();
    return (FutureBuilder<String>(
      future: _calculation, // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        List<Widget> children;
        if (snapshot.hasData) {
          children = <Widget>[];
        } else if (snapshot.hasError) {
          children = <Widget>[
            Icon(
              Icons.error_outline,
              color: Colors.red,
              size: 60,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Text('Error: ${snapshot.error}'),
            )
          ];
        } else {
          return Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 50.0),
            child: SizedBox(
              child: CircularProgressIndicator(),
              width: 60,
              height: 60,
            ),
          ));
        }
        return ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.all(10.0),
                  child: Text("Mis Establecimientos"),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.70,
                height: MediaQuery.of(context).size.width * 0.13,
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                  color: Colors.lightBlue,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: DropdownButton<String>(
                  value: _estatus,
                  hint: Text(
                    "Todos",
                    style: TextStyle(color: Colors.black45),
                  ),
                  onChanged: (String data) {
                    setState(() {
                      _estatusFiltro = data;
                      _estatus = data;
                      Timer.periodic(Duration(seconds: 2), (timer) {
                        setState(() {
                          _recomendados;
                        });
                        timer.cancel();
                      });
                    });
                  },
                  items:
                      estatusAr.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: SizedBox(child: Center(child: Text(value))),
                    );
                  }).toList(),
                  isExpanded: true,
                ),
              ),
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.45,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: TextField(
                      readOnly: true,
                      onChanged: (value) => setState(() {
                        _recomendados;
                      }),
                      controller: dateController,
                      decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.calendar_today),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                      ),
                      onTap: () async {
                        var date = await showDatePicker(
                            locale: Locale("es", "ES"),
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2100));
                        dateController.text =
                            "${date.day}/${date.month}/${date.year}";
                        setState(() {
                          _recomendados;
                        });
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.45,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: TextField(
                      readOnly: true,
                      onChanged: (value) => setState(() {
                        _recomendados;
                      }),
                      controller: dateController2,
                      decoration: const InputDecoration(
                        prefixIcon: const Icon(Icons.calendar_today),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                      ),
                      onTap: () async {
                        var date = await showDatePicker(
                            locale: Locale("es", "ES"),
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2100));
                        dateController2.text =
                            "${date.day}/${date.month}/${date.year}";
                        setState(() {
                          _recomendados;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            _ListaRecomendados(_recomendados)
          ],
        );
      },
    ));
  }
}

class _ListaRecomendados extends StatefulWidget {
  @override
  List<recomendados> _recomendados;

  _ListaRecomendados(this._recomendados);

  _ListaState createState() => _ListaState();
}

class _ListaState extends State<_ListaRecomendados> {
  buscarcambios() {
    Timer.periodic(Duration(seconds: 2), (timer) {
      setState(() {
        widget._recomendados;
      });
      timer.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (buscar == "1") {
      buscarcambios();
      buscar = '0';
    }

    // TODO: implement build
    return Column(
      children: widget._recomendados
          .map((data) => Card(
                child: InkWell(
                  onTap: () => {
                    Navigator.of(context).push(MaterialPageRoute(
                        //this.id,this.nombre,this.direccion,this.telefono
                        builder: (context) => editrecomendado(data.id,
                            data.nombre, data.direccion, data.telefono)))
                  },
                  child: ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("${data.nombre}"),
                        Text("${data.actualizacion}")
                      ],
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${data.estatus}"),
                        Text("Comision: ${data.comision}"),
                      ],
                    ),
                  ),
                ),
              ))
          .toList(),
    );
  }
}
