import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'constants.dart' as globals;
import 'main.dart';


class facturas_externas extends StatefulWidget {

  var idcliente;

  facturas_externas(this.idcliente);

  @override
  facturas_externasState createState() => facturas_externasState();
}

class facturas_externasState extends State<facturas_externas> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Facturas"),
      ),
      body: Center(
          child: ListView(
              children: [MyCustomForm("${widget.idcliente}")]
          )
      ),
    );
  }
}


class MyCustomForm extends StatefulWidget {
  var idcliente;

  MyCustomForm(this.idcliente);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}
// Create a corresponding State class, which holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();
  String total = "";
  String concepto = "";
  var dateController = TextEditingController();
  var date = DateTime.now();
  PickedFile _imageFile;
  void initState() {
    super.initState();
    dateController =
        TextEditingController(text: "${date.day}/${date.month}/${date.year}");
  }
  File _image;
  final picker = ImagePicker();

  Future _getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future _getImageFromGallery() async {
    try {
      final pickedFile = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
        } else {
          print('No image selected.');
        }
      });
    } catch (e) {
      print(e.toString());
    }
  }




  _Addexterna(BuildContext Context) async {
    http.post("${globals.constants.urlApi}api/cliente/saveexterno",
        headers: {
          "Accept": "application/json",
        },
        body: {"concepto": "${concepto}",
          "total": "${total}",
          "fecha": "${dateController.text}",
          "idusuario": "${globals.constants.iduser}",
          "idcliente": "${widget.idcliente}",
          "foto": "${base64Encode(_image.readAsBytesSync())}"
        }
    ).then((http.Response response) {
      var data = jsonDecode(response.body);
      print(data);
    });
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return
      Form(
        key: _formKey,
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: <Widget>[
        Divider(
        color: Colors.white,
          height: 20,
          thickness: 5,
          indent: 20,
          endIndent: 0,
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
              decoration: BoxDecoration(color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: TextFormField(
                decoration: const InputDecoration(
                  prefixIcon: const Icon(Icons.monetization_on_rounded),
                  hintText: 'Total..',
                  labelText: 'Total',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingresa un total';
                  }
                  return null;
                },
                onChanged: (text) {
                  total = text;
                },
              )
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
              decoration: BoxDecoration(color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: TextFormField(
                decoration: const InputDecoration(
                  prefixIcon: const Icon(Icons.apps),
                  hintText: 'Concepto...',
                  labelText: 'Concepto',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingresa un concepto valido';
                  }
                  return null;
                },
                onChanged: (text) {
                  concepto = text;
                },
              )
          ),
        ),
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius:
                BorderRadius.all(Radius.circular(20))),
            child: TextField(
              readOnly: true,
              controller: dateController,
              decoration: const InputDecoration(
                prefixIcon: const Icon(Icons.calendar_today),
                border: OutlineInputBorder(
                    borderRadius:
                    BorderRadius.all(Radius.circular(20))),
              ),
              onTap: () async {
                var date = await showDatePicker(
                    locale: Locale("es", "ES"),
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(1900),
                    lastDate: DateTime(2100));
                dateController.text =
                "${date.day}/${date.month}/${date.year}";
              },
            ),
          ),
        ),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                    BorderRadius.all(Radius.circular(20))),
                child: IconButton(
                    icon: Icon(Icons.camera_alt,color:Colors.black45),
                  onPressed:()=>{_getImage()}

                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                    BorderRadius.all(Radius.circular(20))),
                child: IconButton(
                    icon: Icon(Icons.image,color:Colors.black45),
                    onPressed:()=>{ _getImageFromGallery()}

                ),
              ),
            ),
          ],
        ),
            Center(
              child: _image == null
                  ? Text('Sin seleccionar.')
                  : Image.file(_image),
            ),
          new Container(
              padding: const EdgeInsets.all(20.0),
              child: Center(
                child: new RaisedButton(
                  child: const Text('Guardar'),
                  shape: StadiumBorder(),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      _Addexterna(context);
                      globals.constants.ejecuto = 0;
                      Scaffold.of(context)
                          .showSnackBar(
                          SnackBar(content: Text('Guardando...')));
                    }
                  },
                ),
              )),
          ],
        ),
      );
  }
}

