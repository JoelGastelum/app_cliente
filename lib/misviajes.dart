import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_new/misfacturas.dart';
import 'package:flutter_app_new/show_alert_dialog.dart';
import 'package:http/http.dart' as http;
import 'constants.dart' as globals;
import 'package:expandable/expandable.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'main.dart';
String buscar='0';

String rfcFiltro;
class misviajes extends StatefulWidget {
  @override
  misviajesState createState() => misviajesState();
}

class misviajesState extends State<misviajes> {

  List<String> RFCs = ['Contribuyentes'];
  String rfc;

  initState(){
    rfc = 'Contribuyentes';
  }
  @override
  Widget build(BuildContext context) {
    if (RFCs.length == 1) {
      http.post("${globals.constants.urlApi}api/user/clientes", headers: {
        "Accept": "application/json",
      }, body: {
        "idusuario": "${globals.constants.iduser}"
      }).then((http.Response response) {
        var datos = jsonDecode(response.body);
        var data = datos['clientes'];
        for (int k = 0; k < data.length; k++) {
          if (!RFCs.contains(data[k]['rfc'])) {
            RFCs.insert(k + 1, "${data[k]['rfc']}");
          }
        }
      });
    }
    // TODO: implement build
    return Scaffold(
      drawer: MenuLateral(),
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () =>
            {
              globals.Logout(context),
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ))
            },
          ),
        ],
        centerTitle: true,
        title: Text("Facturar"),
      ),
      body: listaviajes(),
      floatingActionButton: FloatingActionButton(
          onPressed: () => showAlertDialog(context),
          backgroundColor: Colors.blue,
          child: Icon(Icons.add)
      ),
    );
  }

  void showAlertDialog(BuildContext context) {
    String nombre;
    String presupuesto;
    _guardarViaje() {
      http.post(
          "${globals.constants.urlApi}api/cliente/newviaje", headers: {
        "Accept": "application/json",
      }, body: {
        "idusuario": "${globals.constants.iduser}",
        "descripcion": "${nombre}",
        "presupuesto": "${presupuesto}",
        "rfcContribuyente":"${rfcFiltro}"
      }).then((http.Response response) {
        var data = jsonDecode(response.body);
        print(data);
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => misviajes(),
        ));
      });
    }
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomAlertDialog(
          content: Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width / 2.0,
              height:  MediaQuery
                  .of(context)
                  .size
                  .width / 1.2,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
              ),
              child: Column(
                children: [
                  Container(
                      decoration: BoxDecoration(color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: const Icon(Icons.airplanemode_active),
                          hintText: 'Nombre viaje...',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(
                                  20))),
                        ),
                        onChanged: (text) {
                          nombre = text;
                        },
                      )
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Container(
                        decoration: BoxDecoration(color: Colors.white,
                            borderRadius: BorderRadius.all(
                                Radius.circular(20))),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            prefixIcon: const Icon(
                                Icons.monetization_on_rounded),
                            hintText: 'Presupuesto...',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(20))),
                          ),
                          onChanged: (text) {
                            presupuesto = text;
                          },
                        )
                    ),
                  ),
                  dropdownButton(RFCs),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 8.0, bottom: 3.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(20))),
                        child: RaisedButton(
                          shape: StadiumBorder(),
                          child: Text("Guardar"),
                          color: Colors.green,
                          onPressed: () =>
                          {_guardarViaje(), Navigator.pop(context)},
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(bottom: 3.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(20))),
                        child: RaisedButton(
                          child: Text("Cerrar"),
                          shape: StadiumBorder(),
                          color: Colors.redAccent,
                          onPressed: () => {Navigator.pop(context)},
                        ),
                      ),
                    ],
                  ),
                ],
              ) //Contents here
          ),
        );
      },
    );
  }
}



class viajes {
  String name;
  int index;
  String estatus;
  String presupuesto;
  String comprobado;
  String faltante;

  viajes(this.name, this.index, this.estatus, this.presupuesto, this.comprobado,
      this.faltante);
}

class dropdownButton extends StatefulWidget{
  var listarfc;
  dropdownButton(this.listarfc);
  @override
  dropdownButtonState createState() => dropdownButtonState();
}

class dropdownButtonState extends State<dropdownButton>{

 String  rfc;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return                   Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          color: Colors.lightBlue,
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: DropdownButton<String>(
          value: rfc,
          hint: Text(
            "Contribuyentes",
            style: TextStyle(color: Colors.black45),
          ),
          onChanged: (String data) {
            setState((){
              print(data);
              rfc=data;
            rfcFiltro=data;
            });
          },
          items: widget.listarfc.map<DropdownMenuItem<String>>(
                  (String value) {
                    print(value);
                return DropdownMenuItem<String>(
                  value: value,
                  child:
                  SizedBox(child: Center(child: Text(value))),
                );
              }).toList(),
          isExpanded: true,
        ),
      ),
    );
  }

}

class listaviajes extends StatefulWidget {
  @override
  listaviajesState createState() => listaviajesState();
}

class listaviajesState extends State<listaviajes> {
  Future<String> _calculation =
  Future.delayed(Duration(milliseconds: 5000)).then((_) {
    //SE HACE LA LLAMADA API
    return "lata ld";
  });
  var _estatusArr = ["Todos", "Abierto", "Cerrado"];

  var _estatus = "Todos";
  var _estatusFiltro = "Todos";
  List<viajes> _viajes = [];
  var idviaje;

  Future<String> _GetFacturas() {
    _viajes.clear();
    http.post("${globals.constants.urlApi}api/cliente/misviajes", headers: {
      "Accept": "application/json",
    }, body: {
      "idusuario": "${globals.constants.iduser}",
      "estatus": "${_estatus}"
    }).then((http.Response response) {
      var data = jsonDecode(response.body);

      data = data['viajes'];
      //SE CREA UNA LISTA DE WIDGETS PARA INSERTARSE EN PANTALLA
      for (int k = 0; k < data.length; k++) {
        if (data[k]['activo'] == 1) {
          idviaje = data[k]['id'];
        }
        _viajes.insert(k, viajes(
            data[k]['descripcion'], data[k]['id'], "${data[k]['estatus']}",
            "${data[k]['presupuesto']}", "${data[k]['comprobado']}",
            "${data[k]['faltante']}"));
      }
    });

    setState(() {
      _viajes;
    });
    buscar='1';
  }

  @override
  Widget build(BuildContext context) {
    _GetFacturas();
    return (FutureBuilder<String>(
      future: _calculation, // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        List<Widget> children;
        if (snapshot.hasData) {
          children = <Widget>[];
        } else if (snapshot.hasError) {
          children = <Widget>[
            Icon(
              Icons.error_outline,
              color: Colors.red,
              size: 60,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Text('Error: ${snapshot.error}'),
            )
          ];
        } else {
          return Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: SizedBox(
                  child: CircularProgressIndicator(),
                  width: 60,
                  height: 60,
                ),
              ));
        }
        return ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.70,
                height: MediaQuery
                    .of(context)
                    .size
                    .width * 0.13,
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                  color: Colors.lightBlue,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: DropdownButton<String>(
                  value: _estatus,
                  hint: Text(
                    "Contribuyentes",
                    style: TextStyle(color: Colors.black45),
                  ),
                  onChanged: (String data) {
                    setState(() {
                      _estatus = data;
                      _estatusFiltro = data;
                      _viajes;
                    });
                  },
                  items: _estatusArr.map<DropdownMenuItem<String>>(
                          (String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child:
                          SizedBox(child: Center(child: Text(value))),
                        );
                      }).toList(),
                  isExpanded: true,
                ),
              ),
            ),
            viajesBuild(_viajes, idviaje)
          ],
        );
      },
    ));
  }
}


class viajesBuild extends StatefulWidget {
  @override
  List<viajes> _viajes;
  var idviaje;

  viajesBuild(this._viajes, this.idviaje);

  viajesBuildState createState() => viajesBuildState();

}

class viajesBuildState extends State<viajesBuild> {
  var idviaje;

  initState() {
    idviaje = widget.idviaje;
  }

  buscarcambios() {
    Timer.periodic(Duration(seconds: 2), (timer) {
      setState(() {
        widget._viajes;
      });
      timer.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    if(buscar=='1'){
      buscarcambios();
      buscar='0';
    }
    // TODO: implement build
    return Column(
      children:
      widget._viajes.map((data) =>
          Card(
            child: ListTile(
              leading:
              Radio(
                groupValue: idviaje,
                value: data.index,
                onChanged: (val) {
                  setState(() {
                    idviaje = data.index;
                    http.post(
                        "${globals.constants.urlApi}api/cliente/activarviaje",
                        headers: {
                          "Accept": "application/json",
                        }, body: {
                      "idusuario": "${globals.constants.iduser}",
                      "idviaje": "${idviaje}"
                    }).then((http.Response response) {
                      var data = jsonDecode(response.body);
                      print(data);
                    });
                  });
                },
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("${data.name}"),
                  if(data.estatus == "Abierto")
                    IconButton(
                      icon: Icon(Icons.lock_open_rounded),
                      onPressed: () =>
                      {
                        setState(() {
                      http.post(
                      "${globals.constants.urlApi}api/cliente/cambiarestatus",
                      headers: {
                      "Accept": "application/json",
                      }, body: {
                      "idusuario": "${globals.constants.iduser}",
                      "idviaje": "${data.index}"
                      }).then((http.Response response) {
                      var data = jsonDecode(response.body);
                      print(data);
                      });

                          data.estatus = "Cerrado";
                        })
                      },
                    )
                  else
                    IconButton(icon: Icon(Icons.lock),
                      onPressed: () =>
                      {
                        setState(() {
                          http.post(
                              "${globals.constants.urlApi}api/cliente/cambiarestatus",
                              headers: {
                                "Accept": "application/json",
                              }, body: {
                            "idusuario": "${globals.constants.iduser}",
                            "idviaje": "${data.index}"
                          }).then((http.Response response) {
                            var data = jsonDecode(response.body);
                            print(data);
                          });
                          data.estatus = "Abierto";
                        })
                      },
                    )
                ],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Presupuesto:  ${data.presupuesto}"),
                    ],
                  ),
                  Column(
                    children:[
                      Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children:[
                        Text("Comprobado: "),
                          if(data.comprobado!='null')
                          Text("${data.comprobado}")
                          else
                          Text("0.00"),
                        ]
                      )
                    ]
                  ),
                  Column(
                      children:[
                        Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children:[
                                Text("Faltante:          "),
                              if(data.faltante!='null')
                                Text("${data.faltante}")
                              else
                                Text("0.00"),
                            ]
                        )
                      ]
                  )
                ],
              ),

            ),
          )).toList(),
    );
  }
}